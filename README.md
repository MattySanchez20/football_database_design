# Football Database Design

## Scenario

A company organises a 6-a-side football tournament throughout the autumn and a database system is needed to manage the details of teams, players, fixture,and results. For simplicity, each team will play against each other team twice in the tournament (home and away games). The team that has the highest points by the end of the tournament will be the champion.Most standard rules for 6-a-side football are applied. Here is some key information:
- There are 4 teams formed
- There are 8 groups participating and consultants from each groupcan play for any team, but for one team only 
- 3, 1 and 0 points are awarded for a win, draw or lost game respectively
- There are  three  football  pitches  that  can  be  used;  the  availability  will  be  assigned according to the fixtures at the beginning of the tournament
- All  the  raw  data  about  the  tournament,  the  students  and  teams  are  given  in  Section  2. Also, the results of this year’s tournament are given in Section 2


## Testing Database

Test your database implementation by answering the following test cases correctly:

- Listing all students, who play for a particular department (i.e. Cohort 4group)
- Listing all fixtures for a specific date (i.e. 29th of October 2022)
- Listing all the players who have scored more than 2 goals
- Return the total number of goals scored in the season
- Return the number of goals in favour, goals against, goals difference and points by team
- Listing the number of cards (yellow and red) per team
- Return the games that are going to be played

## Results

### ERD

![ERD](ER_Diagram.png)

### Queries

Queries and Creation of tables can be found [here](footy_sql_queries.sql) and [here](footy_db.sql) respectively.
