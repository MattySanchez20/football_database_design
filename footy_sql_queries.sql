-- TASK 3

--Question 1

SELECT player_name, group_name
FROM players
JOIN groups 
ON players.group_id = groups.group_id
order by group_name;

SELECT player_name, group_name
FROM players
JOIN groups 
ON players.group_id = groups.group_id
WHERE group_name ilike '%cohort 6%'
order by group_name;

--Question 2

SELECT tbl1.date, tbl1.venue_name, clubs.club_name, tbl1.home_away_status
FROM (SELECT fixtures.date, venues.venue_name, results.club_id, results.home_away_status
from fixtures
JOIN venues ON fixtures.venue_id = venues.venue_id
JOIN results ON fixtures.fixture_id = results.fixture_id
WHERE date = '2022-10-29') as tbl1
JOIN clubs ON tbl1.club_id = clubs.club_id;

-- Question 3 

SELECT players.player_name, count(goals.goal_id) as total_goals
from goals 
join players on goals.player_id = players.player_id
group by players.player_name
HAVING  count(goals.goal_id) > 2
order by  count(goals.goal_id) DESC ;

--Question 4

SELECT count(goal_id)
from goals;

--Question 5 

SELECT tb2.club_name, tb_for.goals_for, tb2.goals_against
FROM (
	SELECT clubs.club_name, COUNT(*) AS goals_for
	FROM players
	JOIN goals ON goals.player_id = players.player_id
	JOIN clubs ON clubs.club_id = players.club_id
	GROUP BY clubs.club_name) AS tb_for
JOIN (
	SELECT clubs.club_name, COUNT(*) AS goals_against
	FROM clubs
	JOIN goals ON goals.opp_club_id = clubs.club_id
	GROUP BY clubs.club_name) AS tb2 ON tb2.club_name = tb_for.club_name;

--Question 6

SELECT yellows.club_name, coalesce(yellows.count, 0) as yellow_cards,  coalesce(reds.count, 0) as red_cards
from (SELECT   tbl2.club_name, count(cards.card_id)
from cards
join (SELECT players.player_id, clubs.club_name
from players
join clubs on players.club_id = clubs.club_id) as tbl2
on cards.player_id = tbl2.player_id
where cards.colour_id=1
group by tbl2.club_name) as yellows
left join (SELECT   tbl2.club_name, count(cards.card_id)
from cards
join (SELECT players.player_id, clubs.club_name
from players
join clubs on players.club_id = clubs.club_id) as tbl2
on cards.player_id = tbl2.player_id
where cards.colour_id=2
group by tbl2.club_name) as reds
on yellows.club_name = reds.club_name;

--Question 7

SELECT results.result_id, results.fixture_id, fixtures.date, clubs.club_name, results.home_away_status,results.wld
from results
join clubs on results.club_id = clubs.club_id
join fixtures on results.fixture_id  = fixtures.fixture_id
where wld ilike '%tbd%'
order by result_id asc;




