DROP TABLE cards, clubs, colours, fixtures, goals, groups, players, points, results, venues

CREATE TABLE clubs (
	club_id serial PRIMARY KEY,
	club_name CHARACTER VARYING(200) NOT NULL /* CAN'T BE EMPTY */
);

CREATE TABLE venues (
	venue_id serial PRIMARY KEY,
	venue_name CHARACTER VARYING(200) NOT NULL
);

CREATE TABLE groups (
	group_id serial PRIMARY KEY,
	group_name CHARACTER VARYING(200) NOT NULL
);

CREATE TABLE players (
	player_id serial PRIMARY KEY,
	player_name CHARACTER VARYING(200) NOT NULL,
	club_id INTEGER NOT NULL,
	group_id INTEGER NOT NULL,
	FOREIGN KEY (club_id) REFERENCES clubs(club_id),
	FOREIGN KEY (group_id) REFERENCES groups(group_id)
);

CREATE TABLE goals (
	goal_id serial PRIMARY KEY,
	fixture_id INT NOT NULL,
	player_id INT NOT NULL,
	opp_club_id INT NOT NULL,
	FOREIGN KEY (fixture_id) REFERENCES fixtures(fixture_id),
	FOREIGN KEY (player_id) REFERENCES players(player_id),
	FOREIGN KEY (opp_club_id) REFERENCES clubs(club_id)
);

CREATE TABLE fixtures (
	fixture_id serial PRIMARY KEY,
	date DATE NOT NULL,
	venue_id INT NOT NULL,
	FOREIGN KEY (venue_id) REFERENCES venues(venue_id)
);

CREATE TABLE cards (
	card_id serial PRIMARY KEY,
	fixture_id INT NOT NULL,
	player_id INT NOT NULL,
	opp_club_id INT NOT NULL,
	colour_id INT NOT NULL,
	FOREIGN KEY (fixture_id) REFERENCES fixtures(fixture_id),
	FOREIGN KEY (opp_club_id) REFERENCES clubs(club_id),
	FOREIGN KEY (player_id) REFERENCES players(player_id),
	FOREIGN KEY (colour_id) REFERENCES colours(colour_id)
);

CREATE TABLE colours (
	colour_id serial PRIMARY KEY,
	colour VARCHAR(200) NOT NULL
);

CREATE TABLE points (
	WLD VARCHAR(200) PRIMARY KEY,
	points INT
);

CREATE TABLE results (
	result_id serial PRIMARY KEY,
	fixture_id INT,
	club_id INT,
	home_away_status VARCHAR(200),
	WLD VARCHAR(200),
	FOREIGN KEY (fixture_id) REFERENCES fixtures(fixture_id),
	FOREIGN KEY (club_id) REFERENCES clubs(club_id),
	FOREIGN KEY (WLD) REFERENCES points(WLD)
);

COPY clubs FROM 'C:\Program Files\PostgreSQL\14\bin\s.csv' DELIMITER ',' CSV;

COPY groups FROM 'C:\Program Files\PostgreSQL\14\bin\groupsproper.csv' DELIMITER ',' CSV;

COPY venues FROM 'C:\Program Files\PostgreSQL\14\bin\venue.csv' DELIMITER ',' CSV;

COPY points FROM 'C:\Program Files\PostgreSQL\14\bin\points3.csv' DELIMITER ',' CSV;

COPY fixtures FROM 'C:\Program Files\PostgreSQL\14\bin\fixtures.csv' DELIMITER ',' CSV;

COPY players FROM 'C:\Program Files\PostgreSQL\14\bin\players_test.csv' DELIMITER ',' CSV;

COPY goals FROM 'C:\Program Files\PostgreSQL\14\bin\goals.csv' DELIMITER ',' CSV;

COPY colours FROM 'C:\Program Files\PostgreSQL\14\bin\colours.csv' DELIMITER ',' CSV;

COPY cards FROM 'C:\Program Files\PostgreSQL\14\bin\cards.csv' DELIMITER ',' CSV;

COPY results FROM 'C:\Program Files\PostgreSQL\14\bin\results2.csv' DELIMITER ',' CSV; /* not yet */

SELECT *
FROM points;